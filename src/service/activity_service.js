import axios from 'axios'

const BASE_CONFIG = {
    // host: 'http://192.168.82.133:8888'
    host: 'https://explorer.eosforce.io'
}

export const get_info = (page = 1, step = 10) => {
    return new Promise((resolve, reject) => {
        let url = BASE_CONFIG.host + '/v1/chain/get_info';
        axios.post(url, {})
        .then((res) => {
            resolve(res.data);
        });
    });
}

// /web/get_bps
export const get_bps = () => {
    return new Promise((resolve, reject) => {
        let url = '/web/get_bps';
        axios.post(url, {})
        .then((res) => {
            resolve(res.data);
        });
    });
}

export const get_vote_user_num = () => {
    return new Promise((resolve, reject) => {
        let url = '/web/get_vote_user_num';
        axios.post(url, {})
        .then((res) => {
            resolve(res.data);
        });
    });
}

export const get_transaction_total = () => {
    return new Promise((resolve, reject) => {
        let url = '/web/get_transaction_total';
        axios.post(url, {})
        .then((res) => {
            resolve(res.data);
        });
    });
}

export const get_last_info = () => {
    return new Promise((resolve, reject) => {
        axios.post('/web/get_last_info', {})
        .then((res) => {
            resolve(res.data);
        });
    });
}