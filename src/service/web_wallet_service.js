import Eos from 'eosjs'
import BigNumber from 'bignumber.js';

import {
    toAsset,
    square_num
} from '../components/utils/utils.js'
import {option} from './eos_service.js'
// http://47.98.238.9:15001
const test_node = true
const httpEndpoint = option.httpEndpoint;
const network = {
    protocol: 'http',
    blockchain: 'eos',
    host: '47.98.238.9',
    host: httpEndpoint.split('://')[1].split(':')[0],
    port: httpEndpoint.split('://')[1].split(':')[1],
    chainId: 'e2e441f0bd68c735f0f54f2ae9a0e783bb9d76368ba43b9eac486b97deca92b4',
}

const scatter_res = {
    'eos': null,
    'account_name': null,
    'permission': null,
    'has_reject': false,
    'is_running': false
}
/*
    static constexpr uint64_t char_to_symbol( char c ) {
      if( c >= 'a' && c <= 'z' )
         return (c - 'a') + 6;
      if( c >= '1' && c <= '5' )
         return (c - '1') + 1;
      return 0;
   }

   // Each char of the string is encoded into 5-bit chunk and left-shifted
   // to its 5-bit slot starting with the highest slot for the first char.
   // The 13th char, if str is long enough, is encoded into 4-bit chunk
   // and placed in the lowest 4 bits. 64 = 12 * 5 + 4
   static constexpr uint64_t string_to_name( const char* str )
   {
      uint64_t name = 0;
      int i = 0;
      for ( ; str[i] && i < 12; ++i) {
          // NOTE: char_to_symbol() returns char type, and without this explicit
          // expansion to uint64 type, the compilation fails at the point of usage
          // of string_to_name(), where the usage requires constant (compile time) expression.
           name |= (char_to_symbol(str[i]) & 0x1f) << (64 - 5 * (i + 1));
       }

      // The for-loop encoded up to 60 high bits into uint64 'name' variable,
      // if (strlen(str) > 12) then encode str[12] into the low (remaining)
      // 4 bits of 'name'
      if (i == 12)
          name |= char_to_symbol(str[12]) & 0x0F;
      return name;
   }
*/
const char_to_symbol = (c) => {
    let code_c = (c + '').charCodeAt(),
        code_a = 'a'.charCodeAt(),
        code_z = 'z'.charCodeAt(),
        code_1 = '1'.charCodeAt(),
        code_5 = '5'.charCodeAt(),
        res = 0;
    if( code_c >= code_a && code_c <= code_z )
         res = (code_c - code_a) + 6;
    if( code_c >= code_1 && code_c <= code_5 )
         res = (code_c - code_1) + 1;
     console.log(c, res, 'char_to_symbol');
      return res
}
var get_max_pos = (ten_num) => {
    let p_n = new Array(64), res = -1;
    for (let i of p_n.keys()) {
        p_n[i] = 2 ** i;
    }
    for (let _index of p_n.keys()) {
        if (ten_num >= p_n[_index] && ten_num < p_n[_index + 1]) {
            res = _index;
            break ;
        }
    }
    return res;
}
var ten_to_bit = (ten_num) => {
    let p_n = new Array(64), res = new Array(64);
    for (let i of p_n.keys()) {
        p_n[i] = 2 ** i;
        res[i] = 0;
    }
    while (ten_num > 0) {
        let pos = get_max_pos(ten_num);
        ten_num -= 2 ** pos;
        res[pos] = 1;
    }
    return res;
}
const move_bite = (bit_num, move_pos) => {
    let new_zero_array = new Array(move_pos);
    for(let _index of new_zero_array.keys()) { new_zero_array[_index] = 0 };
    console.log( new_zero_array );
    bit_num.splice(0, 0, ...new_zero_array);
    return bit_num;
}
const or_bits = (bit_num_from, bit_num_to) => {
    let total = new BigNumber(0);
    for (let _i of bit_num_from.keys()) {
        if (bit_num_from[_i] || bit_num_to[_i]) {
            bit_num_from[_i] = 1;
        }else{
            bit_num_from[_i] = 0;
        }
    }
    return bit_num_from;
}
const bits_to_ten = (bit_num) => {
    let total = new BigNumber(0), keys = bit_num.keys();
    for(let item of keys){
        if ( bit_num[item] ) {
            total = total.plus(square_num(2, item))
        }
    }
    return total;
}
const string_to_name = ( _str ) => {
    let int_num = parseInt(_str);
    if (!int_num || ('' + int_num ).length != (_str + '').length) {
        return _str;
    }
    let name = new Array(64);
    for(let _i of name.keys()) { name[_i] = 0 };
    let i = 0;
    for ( i ; _str[i] && i < 12; ++i) {
       let symbol = ten_to_bit((char_to_symbol(_str[i]) & 0x1f )),
       pos = 64 - 5 * (i + 1) - 1;
       let moved_bites = move_bite(symbol, pos);
       or_bits(name, moved_bites);
   }
   if (i == 12){
      or_bits(name, ten_to_bit((char_to_symbol(_str[12]) & 0x1f )));
   }
   return bits_to_ten(name).toString();
}
// test_new_eosforce_code
export const get_scatter_identity = async (exchange_account = false) => {
    if(scatter_res.is_running){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(get_scatter_identity());
            }, 1000);
        });
        return ;
    }
    scatter_res.is_running = true;
    const scatter = window.scatter;
    if(!scatter && !exchange_account){
        scatter_res.is_running = false;
        return {
            is_error: false,
            data: {
                eos: null,
                account_name: null,
                permission: null
            },
            msg: 'no scatter'
        };
    }
    if ( exchange_account ) {
       try{
            await scatter.forgetIdentity(); 
       }catch(__){}
    }
    let identity = await scatter.getIdentity({accounts:[network]}).catch(err => err);
    if(identity.isError){
        scatter_res.has_reject = true;
        scatter_res.error = identity;
        scatter_res.is_running = false;
        return {
            is_error: true,
            msg: identity
        }
    }
    const account = identity.accounts.find(function(acc){
         return acc.blockchain === 'eos';
    });
    let options = {
     authorization: account.name+'@'+account.authority,
     broadcast: true,
     sign: true
    }    
    let eos = scatter.eos(network, Eos,  options, "https");
    scatter_res.is_running = false;
    return {
        is_error: false,
        data: {eos, account_name: account.name, permission: account.authority}
    };
}

const call_scatter = async (call_direct = true) => {
    let {eos, account_name, permission} = scatter_res;
    if(!call_direct && scatter_res.has_reject){
        return {
            is_error: true,
            msg: scatter_res.error
        }
    }
    let is_error = false;
    let error_msg = '';
    if(!eos){
        let identity_res = await get_scatter_identity();
        is_error = identity_res.is_error;
        error_msg = identity_res.msg;
        if(!is_error){
            eos = identity_res.data.eos;
            account_name = identity_res.data.account_name;
            permission = identity_res.data.permission;
            scatter_res.eos = identity_res.data.eos;
            scatter_res.account_name = identity_res.data.account_name;
            scatter_res.permission = identity_res.data.permission;
        }
    }
    if(is_error){
        return {
            is_error,
            msg: error_msg
        };
    }
    return {eos, account_name, permission}
}

export const vote = async (amount = 2, bpname = '', tokenSymbol = 'EOS') => {
    let call_res = await call_scatter();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    let token = await eos.contract('eosio'),
        auth = {
            actor: account_name,
            permission
        }
    return await token.vote(account_name, bpname, toAsset(amount, tokenSymbol), auth)
        .then(data => {
            return {
                is_error: false,
                data
            };
        })
        .catch(err => {
            return {
                is_error: true,
                msg: err
            };
        });
}

export const revote = async (frombp, tobp, restake, tokenSymbol = 'EOS') => {
    let call_res = await call_scatter();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    let token = await eos.contract('eosio'),
        auth = {
            actor: account_name,
            permission
        }
    return await token.revote(account_name, frombp, tobp, toAsset(restake, tokenSymbol), auth)
        .then(data => {
            return {
                is_error: false,
                data
            };
        })
        .catch(err => {
            return {
                is_error: true,
                msg: err
            };
        });
}

/*

let action_res = await token.transaction('eosio', tr => {
    tr.claim(voter, pre_bp_name, auth);
    tr.revotefix(voter, fixed_key, bpname, auth);
  })

*/

export const revote_fix = async (voter, pre_bp_name, fixed_key, bpname, wallet_symbol = 'EOS') => {
  let call_res = await call_scatter();
  if(call_res.is_error) return call_res;
  let {eos, account_name, permission} = call_res;
  let token = await eos.contract('eosio'),
      auth = {
          actor: account_name,
          permission
      };

  let res = await token.transaction('eosio', tr => {
    tr.claim(voter, pre_bp_name, auth);
    tr.revotefix(voter, fixed_key, bpname, auth);
  }).catch(err => {
    return {
      is_error: true,
      msg: err
    }
  });

  return res;
}

export const outfixvote = async (voter, fixed_key, bpname) => {
  let call_res = await call_scatter();
  if(call_res.is_error) return call_res;
  let {eos, account_name, permission} = call_res;
  let token = await eos.contract('eosio'),
      auth = {
          actor: account_name,
          permission
      };

  let res = await token.transaction('eosio', tr => {
      tr.claim(voter, bpname, auth);
      tr.outfixvote(voter, fixed_key, auth);
  }).catch(err => {
    return {
      is_error: true,
      msg: err
    }
  });

  return res;
}

export const vote_fix = async (voter, bpname, type, vote_num, stake_typ = 1, fix_vote, unstaking,wallet_symbol = 'EOS') => {
  let call_res = await call_scatter();
  if(call_res.is_error) return call_res;
  let {eos, account_name, permission} = call_res;
  let token = await eos.contract('eosio'),
      auth = {
          actor: account_name,
          permission
      };

  let res = {};
  if(stake_typ == '3'){
    res = await token.transaction('eosio', tr => {
      let new_vote_num = unstaking <= 0 ? vote_num - 1 : vote_num;
      tr.vote(voter, bpname, toAsset(fix_vote - vote_num, wallet_symbol), auth);
      tr.votefix(voter, bpname, type, toAsset(new_vote_num, wallet_symbol), 2, auth);
    }).catch(err => {
      return {
        is_error: true,
        msg: err
      }
    });
  }else{
    res = await token.votefix(voter, bpname, type, toAsset(vote_num, wallet_symbol), stake_typ, auth).catch(err => {
      return {
        is_error: true,
        msg: err
      }
    });
  }

  return res;
}

export const transfer = async (to = '', amount = 1, memo = '', tokenSymbol = 'EOS') => {
    let call_res = await call_scatter();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    let token = await eos.contract(tokenSymbol === 'EOS' ? 'eosio' : 'eosio.token').then(token => { return token }),
        auth = {
            actor: account_name,
            permission
        };
    return await token.transfer(account_name, to, toAsset(amount), memo, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    }
                })
                .catch(error => {
                    return {
                        is_error: true,
                        msg: error
                    }
                })
}

export const transfer_account = async (active_public_key = '', owner_public_key = '', tokenSymbol = 'EOS') => {
    let call_res = await call_scatter();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    let token = await eos.contract(tokenSymbol === 'EOS' ? 'eosio' : 'eosio.token').then(token => { return token }),
        auth = {
            actor: account_name,
            permission
        };

    return await token.transaction('eosio', tr => {
      tr.updateauth(account_name, 'active', 'owner', active_public_key, auth);
      tr.updateauth(account_name, 'owner', '', owner_public_key, auth);
    })
    .then(data => {
        return {
            is_error: false,
            data
        }
    })
    .catch(error => {
        return {
            is_error: true,
            msg: error
        }
    })
    return await token.transfer(account_name, to, toAsset(amount), memo, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    }
                })
                .catch(error => {
                    return {
                        is_error: true,
                        msg: error
                    }
                })
}

// let action_res = await token.transaction('eosio', tr => {
//     tr.updateauth(name, 'active', 'owner', active_public_key, auth);
//     tr.updateauth(name, 'owner', '', owner_public_key, auth);
//   })

export const unfreeze = async (bpname = '') => {
    let call_res = await call_scatter();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    let auth = {
            actor: account_name,
            permission
        };
    let token = await eos.contract('eosio');
        
    return await token.unfreeze(account_name, bpname, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
};

export const claim = async (bpname) => {
    let call_res = await call_scatter();
    if (call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    let auth = {
            actor: account_name,
            permission
        };
    let token = await eos.contract('eosio');
    return await token.claim(account_name, bpname, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
};

export const new_account = async (new_name, publicKey) => {
    let call_res = await call_scatter();
    if (call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    let creator = account_name;
    let auth = {
            actor: account_name,
            permission
        };
    let token = await eos.contract('eosio');
    return await token.newaccount(creator, new_name, publicKey, publicKey, auth)
                .then(data => {
                    console.log( data );
                })
                .catch(err => {
                    console.log(err);
                });
}

export const get_my_vote_llist = async () => {
    let call_res = await call_scatter(false);
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;

    if(!account_name){
        return {
            is_error: true,
            msg: ''
        };
    }
    return await Eos({ httpEndpoint })
                .getTableRows({ scope: string_to_name(account_name), code: 'eosio', table: 'votes', json: true, limit: 1000 })
                .then(data => {
                    return {
                        is_error: false,
                        data,
                        account_name
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
}

export const get_available = async () => {
    let call_res = await call_scatter(false);
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    if(!account_name){
        return {
            is_error,
            msg: ''
        };
    }
    return await Eos({ httpEndpoint })
                .getTableRows({"scope":"eosio","code":"eosio","table":"accounts","lower_bound":account_name,"limit":1,"json":true})
                .then(data => {
                  console.log('available', data);
                  if(!data.rows.find(i => i.name == account_name)){
                    data.rows = [{
                      name: account_name,
                      available: '0.0000 EOS'
                    }]
                  };
                    return {
                        is_error: false,
                        data,
                        account_name
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
}

export const query_fixed_vote = async (account_name, limit = 1000, lower_bound = '') => {
  let params = {
      json:true,
      scope: account_name,
      code: 'eosio',
      table: 'fixvotes',
      limit,
      lower_bound
  }
  let data = await Eos({ httpEndpoint }).getTableRows(params);
  console.log(data);
  return data;
}

// query_fixed_vote('bandon')

export const get_account = async () => {
  let call_res = await call_scatter(false);
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res;
    if(!account_name){
        return {
            is_error,
            msg: ''
        };
    }
    return await Eos({ httpEndpoint })
                .getAccount(account_name)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
}

// Eos({ httpEndpoint }).getInfo({});

export const get_top_bps = () => {
    return new Promise(async (resolve, reject) => {
        let {last_irreversible_block_num, head_block_num} = await Eos({ httpEndpoint }).getInfo({});
        let {schedule_version} = await Eos({ httpEndpoint }).getBlock({block_num_or_id: last_irreversible_block_num});
        const params = {
          "scope":"bacc",
          "code":"bacc",
          "table":"schedules",
          "lower_bound":schedule_version,
          "json":true,"limit":1000
        };
        let top_bps = await Eos({ httpEndpoint }).getTableRows(params);
        resolve({rows: top_bps.rows[0], schedule_version, head_block_num});
    });
}

export const get_bps = () => {
    return new Promise((resolve, reject) => {
        const params = { 
          scope: 'bacc',
          code: 'bacc',
          table: 'bps',
          json: true, 
          limit: 1000 
        };
        Eos({ httpEndpoint })
        .getTableRows(params)
        .then(async data => {
            resolve({
                is_error: false,
                data: data.rows
            });
        })
        .catch(err => {
            resolve({
                is_error: true,
                msg: err
            });
        })
    });
}


