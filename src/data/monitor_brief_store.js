import { 
    get_info, 
    get_user_num, 
    get_transaction_total,
    get_vote_num,
    get_analytics
} from '../service/eos_service.js'
export default {
    state: {
        info: {
            head_block_num: null
        },
        users_info: {
            total: null
        },
        transaction: {
            total: null
        },
        vote_user_num: {
            total: null
        },
        CONTRACT_ACCOUNT_NUM: {
            total: null
        },
        TOKEN_NUM: {
            total: null
        },
        total_voted: {
            total: null
        },
        total_voted_user_num: {
            total: null
        },
        today_transfer: {
            total: null
        },
        TRANSACTION_NUM: {
            total: null
        },
        ACCOUNT_NUM: {
            total: null
        },
        total_actived_coin: {
            total: null
        },
        origin_assets: 1000000000,
        staked: null
    },
    mutations: {
        add_company (state, data) {
            
        },
        set_staked (state, staked) {
            state.staked = staked;
        }
    },
    actions: {
        async update_info({state, dispatch, commit, rootState, rootGetters}){
            let data = await get_info().catch(error => null);
            if(!data){
                return ;
            }
            state.info.head_block_num = data.head_block_num;
            return data;
        },
        async update_analytics ({state, dispatch, commit, rootState, rootGetters}) {
            let res = await get_analytics().catch(error => {
                return null;
            });
            if(!res){
                return ;
            }
            for(let item of res.data){
                if(state[item.key]){
                    state[item.key]['total'] = item.value;
                }
            }
            return ;
        },
        update_transaction_num({state, dispatch, commit, rootState, rootGetters}){
            return new Promise((reosolve, reject) => {
                get_transaction_total()
                .then(data => {
                    if(data) state.transaction.total = data.data;
                    reosolve(data);
                });
            });
        },
        async update_info_user_transaction({state, dispatch, commit, rootState, rootGetters}){
            await Promise.all([ dispatch('update_info'), dispatch('update_analytics') ] );
        },
        update_vote_user_num({state, dispatch, commit, rootState, rootGetters}){
            return new Promise((reosolve, reject) => {
                get_vote_num()
                .then(data => {
                    if(data) state.vote_user_num.total = data.data;
                    reosolve(data);
                });
            });
        },
        set_staked({state, dispatch, commit, rootState, rootGetters}, staked){
            commit('set_staked', staked);
        }
    }
}
