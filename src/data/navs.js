import { lang } from './lang.js'

export const data = {
    nav_key_name:  [
        {
            'key': 'block_view',
            'name': lang.data['blocks']
        },
        {
            'key': 'transaction_view',
            'name': lang.data['transactions']
        },
        {
            'key': 'accounts_view',
            'name': lang.data['accounts']
        },
        {
            'key': 'contract_account_list',
            'name': lang.data['contract_name']
        },
        // {
        //     'key': 'account_token_list',
        //     'name': lang.data['account_token']
        // },
        {
            'key': 'web_wallet_view',
            'name': lang.data['web_wallet_view']
        }
    ],
    navs:[
        {
            key: 'monitor_view',
            name: '监控区',
            child: [
                {
                    key: 'block_view',
                    name: '区块',
                    child: [
                        {
                            key: 'block_detail_view',
                            name: '区块详情'
                        }
                    ]
                },
                {
                    key: 'transaction_view',
                    name: '交易',
                    child: [
                        {
                            key: 'transaction_detail_view',
                            name: '交易详情'
                        }
                    ]
                },
                {
                    key: 'accounts_view',
                    name: '账户',
                    child: [
                        {
                            key: 'account_detail_view',
                            name: '账户详情'
                        }
                    ]
                },
                {
                    key: 'web_wallet_view',
                    name: '投票',
                    child: [
                        {
                            key: 'web_wallet_view',
                            name: '投票'
                        }
                    ]
                }
            ]
        }
    ]
}
